class Observable {
    constructor() {
        this.listeners = [];
    }

    listen(listener) {
        this.listeners.push(listener);
    }
    notify() {
        for (let callable of this.listeners) {
            callable();
        }
    }
    unsubscribe(listener){
        let index = 0;
        while (index != -1) {
            index = this.listeners.indexOf(listener);
            if (index >= 0) {
                this.listeners.splice(index, 1);
            }
        }
    }
}