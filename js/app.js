class App {
    run() {
        $(document).ready(() => {
            const store = new Store();
            const form = new TodoForm($('form'), store);
            form.init();
            
            const list = new TodoList($('#list-container'), store);
            list.init();
            list.setOnEdit((todo) => {
                form.setItem(todo);
            });


        })
    }
}

const app = new App;
app.run();