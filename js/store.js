class Store {
    constructor() {
        this._data = [];
        this.onChange = new Observable();
    }

    all() {
        return this._data;
    }

    create(text) {
        const id = `${Date.now()}`;
        const todo  = {
            id,
            text,
            createdAt: new Date(),
            status: 'new'
        }
        this._data.push(todo);

        return new Promise((res) => {
            this.onChange.notify();
            res(todo);
        });
    }

    read(id) {
        return new Promise((res, rej) => {
            const todo = this._data.find(v => v.id === id);
            if (todo) {
                this.onChange.notify();
                res(todo);
            } else {
                rej(new Error(`Invalid id ${id}`));
            }
        });
    }

    update(id, data) {
        const todo = this._data.find((v) => v.id === id);
        if (todo) {
            for (let key in data) {
                if (todo.hasOwnProperty(key)) {
                    todo[key] = data[key]; 
                }
            }
        }

        return new Promise((res) => {
            this.onChange.notify();
            res(todo);
        });
    }

    delete(id) {
        
        return new Promise((res, rej) => {
            const index = this._data.findIndex(v => v.id === id);
            if (index) {
                this._data.splice(index, 1);
                this.onChange.notify();
                res(true);
            } else {
                rej(new Error(`Invalid id ${id}`));
            }
        });
    }
}