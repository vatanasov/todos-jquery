class TodoForm {
    constructor(form, store) {
        this.form = form;
        this.store = store;
        $(this.form).on('submit', (e) => e.preventDefault());
        this.item = undefined;
    }
    init() {
        $('button', this.form).on('click', () => this.save());
        this.setFormValues();
    }

    setFormValues() {
        if (!this.item) {
            $('#status-container').hide();
            $('#text').val('');
        } else {
            $('#status-container').show();
            $('#status').val(this.item.status);
            $('#text').val(this.item.text);
        }
    }

    save() {
        const status = $('#status').val();
        const text = $('#text').val();

        if (this.item) {
            this.store.update(this.item.id, {status, text});
            this.item = undefined;
        } else {
            this.store.create(text);
        }

        this.setFormValues();      
    }

    setItem(item) {
        this.item = item;
        this.setFormValues();
    }

}