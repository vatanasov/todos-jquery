class TodoList {
    constructor(el, store) {
        this.el = el;
        this.store = store;
        this.onEdit = () => {};
    }

    init() {
        this.store.onChange.listen(() => {
            this.render();
        })
    }

    render() {
        this.el.empty();
        const ul = $('<ul></ul>').appendTo(this.el);
        for (let entry of this.store.all()) {
            const li = $(`
                <li class="${entry.status}">
                    <span>${entry.text}</span>
                    <button class="edit">EDIT</button>
                    <button class="delete">DELETE</button>
                </li>
            `).appendTo(ul);

            $('button.edit', li).on('click', () => {
                this.onEdit(entry);
            });
            
            $('button.delete', li).on('click', () => {
                this.store.delete(entry.id);
            });
        }
    }
    setOnEdit(fn) {
        this.onEdit = fn;
    }
}